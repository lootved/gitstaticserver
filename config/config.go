package config

var GitUrl string
var DirToServe string
var Host string
var Repos map[string]bool

func Read() error {
	GitUrl = "giturl"
	Host = ":3000"
	Repos = make(map[string]bool)
	Repos["abc"] = true
	return nil
}
