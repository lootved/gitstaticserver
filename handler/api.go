package handler

import (
	"ms/goserver/config"

	"github.com/gofiber/fiber/v2"
)

func Update(c *fiber.Ctx) error {
	repos := c.Query("repos")
	if !config.Repos[repos] {
		c.SendStatus(404)
		return c.SendString("Missing or invalid `repos` query parameter")
	}
	return c.SendString("Updating repos")
}

func UpdateHotel(c *fiber.Ctx) error {
	return c.SendString("Updating hotel from file")
}
