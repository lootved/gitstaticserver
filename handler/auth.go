package handler

import "github.com/gofiber/fiber/v2"

func Login(c *fiber.Ctx) error {
	return renderLogin(c, 200, "")
}

func Logout(c *fiber.Ctx) error {
	return c.SendString("logout not implemented")
}

func renderLogin(c *fiber.Ctx, statusCode int, errorMsg string) error {
	c.SendStatus(statusCode)
	return c.Render("login", fiber.Map{"message": errorMsg})
}
